<!DOCTYPE html>
<html lang="en">
<head>
        
        
    <style>html {
        height: 100%;
      }
      .header{
    position:absolute;
    width: 900px;
    height: 500px;
    border-radius: 10px;
    left:19%;
    padding: 20px;
    box-sizing: border-box;
    background: goldenrod;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: rgb(255, 255, 255);
    border-radius: 20px;
    border-right-style: groove;
    border-right-color: #fff;
    border-bottom-style: groove;
    border-bottom-color:#fff;
    
  }
      body {
        margin:0;
        padding:0;
        font-family: sans-serif;
        background:#ecf0f3;
        color: #0d1a4a;
      }
      
      .container {
        position: absolute;
        top: 60%;
        left: 60%;
        width: 400px;
        font-family: 'Courier New', Courier, monospace;
        padding: 40px;
        transform: translate(-50%, -50%);
        /* background:linear-gradient(brown,#141e30); */
        box-sizing: border-box;
        box-shadow: 0 20px 25px rgba(0,0,0,.6);
        border-radius: 20px;
        color:#ffffff ;
        border-top-style: outset;
        border-top-color:white;
        border-left-style:outset;
        border-left-color: goldenrod;
        border-bottom-style: inset;
        border-bottom-color: goldenrod;
      }
      .container .box {
        position: relative;
      }
      
      .container .box input {
        width: 100%;
        padding: 10px 0;
        font-size: 16px;
        color: #ffffff;
        margin-bottom: 30px;
        border: none;
        border-bottom: 1px solid #fff;
        outline: none;
        background: transparent;
      }
      .container .box label {
        position: absolute;
        top:0;
        left: 0;
        padding: 10px 0;
        font-size: 16px;
        color: #fff;
        pointer-events: none;
        transition: .5s;
      }
      
      .container .box input:focus ~ label,
      .container .box input:valid ~ label {
        top: -20px;
        left: 0;
        color: #ffffff;
        font-size: 12px;
      }
      
      .container form a {
        position: relative;
        display: inline-block;
        padding: 10px 90px;
        ;
        font-size: 16px;
        text-decoration: none;
        text-transform: uppercase;
        overflow: hidden;
        transition: .5s;
        margin-top: 40px;
        letter-spacing: 10px;
        background:  goldenrod;
        color: #fff;
      }
      
      .container a:hover {
        background:#ecf0f3;
        color: #02022a;
        border-radius: 5px;
        box-shadow: 0 0 5px ,greenyellow,#3f547f
                    0 0 25px greenyellow,
                    0 0 50px #0d1a4a,#3f547f,
                    0 0 100px #141e30;
      }
      
  .container a span {
    position: absolute;
    display: block;
  }
  
  .container a span:nth-child(1) {
    top: 0;
    left: -100%;
    width: 100%;
    height: 70px;
    background: linear-gradient(90deg, transparent, white);
    animation: btn-anim1 3s linear infinite;
  }
  
  @keyframes btn-anim1 {
    0% {
      right: -100%;
    }
    50%,100% {
      left: 100%;
    }
  }/*
  
  .container a span:nth-child(2) {
    top: -100%;
    right: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(180deg, transparent, #141e30);
    animation: btn-anim2 30s linear infinite;
    animation-delay: .25s
  }
  
  @keyframes btn-anim2 {
    0% {
      top: -100%;
    }
    50%,100% {
      top: 100%;
    }
  } */
  
  /* .container a span:nth-child(3) {
    bottom: 0;
    right: -100%;
    width: 100%;
    height: 70px;
    background: linear-gradient(270deg, transparent, #WHITE);
    animation: btn-anim3 2s linear infinite;
    animation-delay: .5s
  }
  
  @keyframes btn-anim3 {
    0% {
      right: -100%;
    }
    50%,100% {
      right: 100%;
    }
  } */
  
  /* .container a span:nth-child(4) {
    bottom: -100%;
    left: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(360deg, transparent, #141e30);
    animation: btn-anim4 30s linear infinite;
    animation-delay: .75s
  }
  
  @keyframes btn-anim4 {
    0% {
      bottom: -100%;
    }
    50%,100% {
      bottom: 100%;
    }
  } */
  .submit{
  border-style: hidden;
  background-color: transparent;
  font-size: large;
  font-style:unset;
  color: white;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 10px;
  margin-bottom: 10px;
  letter-spacing:4px;
  
}
.submit:hover{
  color: #04083b;
}
      </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="header">
     <b> QUESTION 23:</b><br><br>
     WAP to calculate and print the Electricity bill of a given customer. The customer id., name and unit consumed by the user should be taken from the keyboard and display the total amount to pay by the customer. The charges are as follow :
     Cost of Energy (Unit) in kw = N22.5. <BR>
     *To be Entered from kyboard* <br>
     -Total Energy Consumed <br>
     -To amount To be paid <br>
     -Outstanding Balance.
     
    </div>
    
        <div class="container">
            <form action="Question23.html">
      
                      
                        <?php
$customer_id = $_POST['customer_id'];
$customer_name = $_POST['customer_name'];
$unit_consumed = $_POST['unit_consumed'];
$outstanding_balance = isset($_POST['outstanding_balance']) ? $_POST['outstanding_balance'] : 0;


$cost_per_unit = 22.5;
$total_amount = $unit_consumed * $cost_per_unit;
$final_amount = $total_amount + $outstanding_balance;
?>


    <h2>Electricity Bill Summary</h2>
    <p><strong>Customer ID:</strong> <?php echo $customer_id; ?></p>
    <p><strong>Customer Name:</strong> <?php echo $customer_name; ?></p>
    <p><strong>Units Consumed:</strong> <?php echo $unit_consumed; ?> kWh</p>
    <p><strong>Total Amount:</strong> N<?php echo number_format($total_amount, 2); ?></p>
    <p><strong>Outstanding Balance:</strong> N<?php echo number_format($outstanding_balance, 2); ?></p>
    <p><strong>Final Amount to Pay:</strong> N<?php echo number_format($final_amount, 2); ?></p>


                
              <a href="">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              <input type="submit" value="GO BACK" class="submit">
              </a>
            </form>
              </div>
             
                        
        
    
</body>
</html>